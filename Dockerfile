FROM maven:3.6.3-openjdk-15 AS build  
COPY src /app/src
COPY pom.xml /app
RUN mvn -f /app/pom.xml clean install

FROM openjdk:15.0.2-slim-buster
COPY --from=build /app/target/beer-0.0.1-SNAPSHOT.jar /app/beer-0.0.1-SNAPSHOT.jar  
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app/beer-0.0.1-SNAPSHOT.jar"]