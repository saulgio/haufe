### Test for Haufe

#### Author: Saul Giordani

### Build and run

The Application can be built by running the Docker command:
    >docker build -t beer-demo .

It can be then run by running the Docker command:
    >docker run --rm -it -p 8080:8080 beer-demo:latest

### DB description

The application uses H2 "in memory" DB. At each startup, the DB will be populated with Mock data, as indicated in BeerApplication.java.
The DB can be opened navigating to localhost:8080/h2-console. User is "user" and password is "psw".

### Security and logic

It is securized, a User (Manufacturer) can have 2 roles: ADMIN, MANUFACTURER. It is not possible to add 2 ADMIN users, as by default a new User will be a Manufacturer.
Only an ADMIN user can add Manufacturers and Delete all the Beers in the DB.
A MANUFACTURER user can add beers, edit/delete his/her onw ones and edit/delete his/her own profile.
An image can be uploded for a specific beer and downloaded.

### Documentation

A description of the EPs can be found at localhost:8080/swagger-ui/
Two Postman collections can be found in src/main/resources

A suite of tests can be found in the BeerApplicationTest.java class.