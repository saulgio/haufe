package com.haufe.beer.controller;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import com.haufe.beer.exception.ManufacturerNotFoundException;
import com.haufe.beer.model.domain.Manufacturer;
import com.haufe.beer.model.dto.ManufacturerDto;
import com.haufe.beer.security.AuthorizationHelper;
import com.haufe.beer.service.ManufacturerService;
import com.haufe.beer.validator.NewManufacturerValidator;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/manufacturers")
@Slf4j
public class ManufacturerController {

    @Resource
    private ManufacturerService manufacturerService;

    @Resource
    private ModelMapper mapper;

    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Resource
    private AuthorizationHelper authHelper;

    @ApiOperation(value = "Get all manufacturers", responseContainer = "List", response = ManufacturerDto.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "All manufacturers", response = ManufacturerDto.class, responseContainer = "List"),
            @ApiResponse(code = 204, message = "No manufacturers", response = Void.class) })
    @GetMapping
    public ResponseEntity<List<ManufacturerDto>> getManufacturers(Pageable pageable) {
        log.info("get all manufacturers");
        List<ManufacturerDto> manufacturers = Arrays
                .asList(mapper.map(manufacturerService.getManufacturers(pageable), ManufacturerDto[].class));
        if (manufacturers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(manufacturers, HttpStatus.OK);
        }
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Add new", response = ManufacturerDto.class, code = 201, notes = "If manufacturer is allowed or admin")
    @ApiResponse(code = 201, message = "If success", response = ManufacturerDto.class)
    @PostMapping
    public ResponseEntity<ManufacturerDto> addNew(
            @Validated(NewManufacturerValidator.class) @RequestBody ManufacturerDto manufacturerDto) {
        log.info("add new manufacturer");
        manufacturerDto.setPassword(bCryptPasswordEncoder.encode(manufacturerDto.getPassword()));
        Manufacturer manufacturer = manufacturerService
                .createManufacturer(mapper.map(manufacturerDto, Manufacturer.class));
        return new ResponseEntity<>(mapper.map(manufacturer, ManufacturerDto.class), HttpStatus.CREATED);
    }

    @Secured({ "ROLE_ADMIN", "ROLE_MANUFACTURER" })
    @ApiOperation(value = "Update", response = ManufacturerDto.class, code = 200, notes = "If manufacturer is allowed or admin")
    @ApiResponse(code = 200, message = "If success", response = ManufacturerDto.class)
    @PutMapping("/{id}")
    public ResponseEntity<ManufacturerDto> update(@PathVariable Long id,
            @Valid @RequestBody ManufacturerDto manufacturerDto) {
        log.info("update manufacturer");
        authHelper.byManufacturerId(id);
        manufacturerDto.setId(id);
        Manufacturer manufacturer = manufacturerService
                .updateManufacturer(mapper.map(manufacturerDto, Manufacturer.class));
        return new ResponseEntity<>(mapper.map(manufacturer, ManufacturerDto.class), HttpStatus.OK);
    }

    @ApiOperation(value = "Get manufacturer details", response = ManufacturerDto.class, code = 200)
    @ApiResponses({ @ApiResponse(code = 200, message = "If present", response = ManufacturerDto.class),
            @ApiResponse(code = 500, message = "If not found", response = ManufacturerNotFoundException.class) })
    @GetMapping("/{id}")
    public ResponseEntity<ManufacturerDto> getManufacturerById(@PathVariable Long id) {
        log.info("get manufacturer details by id {}", id);
        return new ResponseEntity<>(mapper.map(manufacturerService.getById(id), ManufacturerDto.class), HttpStatus.OK);
    }

    @Secured({ "ROLE_ADMIN", "ROLE_MANUFACTURER" })
    @ApiOperation(value = "Delete manufacturer by id", response = Void.class, code = 200)
    @ApiResponses({ @ApiResponse(code = 200, message = "If deleted", response = Void.class),
            @ApiResponse(code = 500, message = "If not found", response = ManufacturerNotFoundException.class) })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteManufacturerById(@PathVariable Long id) {
        log.info("delete manufacturer by id {}", id);
        authHelper.byManufacturerId(id);
        manufacturerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
