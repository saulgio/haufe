package com.haufe.beer.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/config")
public class ConfigController {

    @ApiOperation(value = "Get all iso country codes", responseContainer = "List", response = String.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "All iso country codes", response = String.class, responseContainer = "List") })
    @GetMapping("/isoCodes")
    public ResponseEntity<List<String>> getIsoCodes() {
        return new ResponseEntity<>(new ArrayList<>(Locale.getISOCountries(Locale.IsoCountryCode.PART1_ALPHA3)),
                HttpStatus.OK);
    }
}
