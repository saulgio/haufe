package com.haufe.beer.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.haufe.beer.exception.BeerNotFoundException;
import com.haufe.beer.model.domain.Beer;
import com.haufe.beer.model.dto.BeerDto;
import com.haufe.beer.model.dto.BeerFilter;
import com.haufe.beer.model.type.BeerType;
import com.haufe.beer.security.AuthorizationHelper;
import com.haufe.beer.service.BeerService;
import com.haufe.beer.validator.NewBeerValidator;
import com.haufe.beer.validator.UpdateBeerValidator;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/beers")
@Slf4j
public class BeerController {

    @Resource
    private BeerService beerService;

    @Resource
    private ModelMapper mapper;

    @Resource
    private AuthorizationHelper authHelper;

    @ApiOperation(value = "Get all beers", responseContainer = "List", response = BeerDto.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "All beers, if not present in DB, data are retrieved from punkapi", response = BeerDto.class, responseContainer = "List") })
    @GetMapping
    public ResponseEntity<List<BeerDto>> getBeers(Pageable pageable) {
        log.info("get all beers");
        List<BeerDto> beers = convertDomainToDto(beerService.getBeers(pageable));
        if (beers.isEmpty()) {
            int pageNumber = pageable.getPageNumber() + 1; // punkapi doesn't suppor 0 as first page, but 1 instead
            int pageSeze = pageable.getPageSize();
            String pagination = "?page=" + pageNumber + "&per_page=" + pageSeze;
            String uri = "https://api.punkapi.com/v2/beers";
            if (StringUtils.isNotBlank(pagination)) {
                uri += pagination;
            }
            RestTemplate restTemplate = new RestTemplate();
            beers = Arrays.asList(restTemplate.getForObject(uri, BeerDto[].class)).stream().map(beer -> {
                beer.setManufacturerType("EXTERNAL");
                return beer;
            }).collect(Collectors.toList());
        }
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all beers by filter", responseContainer = "List", response = BeerDto.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Filter applied to all beers present in DB", response = BeerDto.class, responseContainer = "List"),
            @ApiResponse(code = 204, message = "If not results", response = BeerDto.class, responseContainer = "List") })
    @GetMapping("/filter")
    public ResponseEntity<List<BeerDto>> getBeersByFilter(@RequestParam(required = false) String name,
            @RequestParam(required = false) Double graduation, @RequestParam(required = false) BeerType beerType,
            @RequestParam(required = false) String nationality, @RequestParam(required = false) Long manufacturerId,
            Pageable pageable) {
        log.info("filter beers");
        List<BeerDto> beers = convertDomainToDto(
                beerService.getBeersByFilter(BeerFilter.builder().name(name).graduation(graduation).beerType(beerType)
                        .nationality(nationality).manufacturerId(manufacturerId).build(), pageable));
        if (beers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(beers, HttpStatus.OK);
        }
    }

    @Secured({ "ROLE_ADMIN", "ROLE_MANUFACTURER" })
    @ApiOperation(value = "Add new", response = BeerDto.class, code = 201, notes = "If manufacturer is allowed or admin")
    @ApiResponse(code = 201, message = "If success", response = BeerDto.class)
    @PostMapping
    public ResponseEntity<BeerDto> addNew(@Validated(NewBeerValidator.class) @RequestBody BeerDto beerDto) {
        log.info("add new beer");
        if (authHelper.isNotAdmin()) {
            beerDto.setManufacturerId(authHelper.getPrincipal().getUserId());
        }
        Beer toSave = mapper.map(beerDto, Beer.class);
        toSave.setType(beerDto.getType().name());
        Beer beer = beerService.createBeer(toSave);
        return new ResponseEntity<>(mapper.map(beer, BeerDto.class), HttpStatus.CREATED);
    }

    @Secured({ "ROLE_ADMIN", "ROLE_MANUFACTURER" })
    @ApiOperation(value = "Update", response = BeerDto.class, code = 200, notes = "If manufacturer is allowed or admin")
    @ApiResponse(code = 200, message = "If success", response = BeerDto.class)
    @PutMapping("/{id}")
    public ResponseEntity<BeerDto> update(@PathVariable Long id,
            @Validated(UpdateBeerValidator.class) @RequestBody BeerDto beerDto) {
        log.info("update beer");
        authHelper.byBeerId(id);
        beerDto.setId(id);
        Beer toSave = mapper.map(beerDto, Beer.class);
        toSave.setType(beerDto.getType().name());
        Beer beer = beerService.updateBeer(toSave);
        return new ResponseEntity<>(mapper.map(beer, BeerDto.class), HttpStatus.OK);
    }

    @Secured({ "ROLE_ADMIN", "ROLE_MANUFACTURER" })
    @ApiOperation(value = "Add image to beer", response = BeerDto.class, code = 200, notes = "If manufacturer is allowed or admin")
    @ApiResponse(code = 200, message = "If success", response = BeerDto.class)
    @PatchMapping(value = "/{id}/image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Void> addImage(@PathVariable Long id,
            @RequestPart("image") MultipartFile image) {
        log.info("update beer");
        authHelper.byBeerId(id);
        beerService.addImage(id, image);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Get beer details", response = BeerDto.class, code = 200)
    @ApiResponses({ @ApiResponse(code = 200, message = "If present", response = BeerDto.class),
            @ApiResponse(code = 500, message = "If not found", response = BeerNotFoundException.class) })
    @GetMapping("/{id}")
    public ResponseEntity<BeerDto> getBeerById(@PathVariable Long id) {
        log.info("get beer details by id {}", id);
        return new ResponseEntity<>(mapper.map(beerService.getById(id), BeerDto.class), HttpStatus.OK);
    }

    @ApiOperation(value = "Get beer image", response = MultipartFile.class, code = 200)
    @ApiResponses({ @ApiResponse(code = 200, message = "If present", response = BeerDto.class),
            @ApiResponse(code = 500, message = "If not found", response = BeerNotFoundException.class) })
    @GetMapping(value = "/{id}/image", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<byte[]> getBeerImageById(@PathVariable Long id) {
        log.info("get beer details by id {}", id);
        return new ResponseEntity<>(beerService.getById(id).getImage(), HttpStatus.OK);
    }

    @Secured({ "ROLE_ADMIN", "ROLE_MANUFACTURER" })
    @ApiOperation(value = "Delete beer by id", response = Void.class, code = 200)
    @ApiResponses({ @ApiResponse(code = 200, message = "If deleted", response = Void.class),
            @ApiResponse(code = 500, message = "If not found", response = BeerNotFoundException.class) })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBeerById(@PathVariable Long id) {
        log.info("delete beer by id {}", id);
        authHelper.byBeerId(id);
        beerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete all beers", response = Void.class, code = 200)
    @ApiResponse(code = 200, message = "Deleted", response = Void.class)
    @DeleteMapping
    public ResponseEntity<Void> deleteAll() {
        log.info("delete all beers");
        beerService.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private List<BeerDto> convertDomainToDto(List<Beer> beers) {
        return beers.stream().map(beer -> {
            BeerDto dto = mapper.map(beer, BeerDto.class);
            dto.setManufacturerId(beer.getManufacturer().getId());
            return dto;
        }).collect(Collectors.toList());
    }
}
