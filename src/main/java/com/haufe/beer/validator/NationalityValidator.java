package com.haufe.beer.validator;

import java.util.Locale;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class NationalityValidator implements ConstraintValidator<NationalityValid, String> {
 
    @Override
    public void initialize(NationalityValid constraint) {
        // init
    }
 
    public boolean isValid(String nationality, ConstraintValidatorContext context) {
        boolean isValid = true;
        if (StringUtils.isNotBlank(nationality)) {
            isValid = Locale.getISOCountries(Locale.IsoCountryCode.PART1_ALPHA3).contains(nationality);
        }
        return isValid;
    }
 
}