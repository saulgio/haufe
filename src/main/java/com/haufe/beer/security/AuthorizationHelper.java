package com.haufe.beer.security;

import javax.annotation.Resource;

import com.haufe.beer.exception.UnauthorizedException;
import com.haufe.beer.service.BeerService;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component("authHelper")
public class AuthorizationHelper {

    @Resource
    private BeerService beerService;
    
    public void byManufacturerId(Long manufacturerId) {
        isPrincipal(manufacturerId);
    }

    public void byBeerId(Long beerId) {
       isPrincipal(beerService.getById(beerId).getManufacturer().getId());
    }

    public UserPrincipal getPrincipal() {
        return (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    private void isPrincipal(Long manufacturerId) {
        if (isNotAdmin() && !getPrincipal().getUserId().equals(manufacturerId)) {
            throw new UnauthorizedException();
        }
    }

    public boolean isNotAdmin() {
        return !getPrincipal().getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }
}
