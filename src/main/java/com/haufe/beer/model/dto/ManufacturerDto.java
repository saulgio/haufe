package com.haufe.beer.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.haufe.beer.validator.NationalityValid;
import com.haufe.beer.validator.NewManufacturerValidator;

import lombok.Data;

@Data
public class ManufacturerDto implements Serializable {

    private static final long serialVersionUID = -273415779759842485L;

    @JsonProperty(access = Access.READ_ONLY)
    private Long id;

    @NotBlank(groups = NewManufacturerValidator.class)
    @JsonProperty(access = Access.WRITE_ONLY)
    private String email;

    @NotBlank(groups = NewManufacturerValidator.class)
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;

    @NotBlank(groups = NewManufacturerValidator.class)
    private String firstname;

    @NotBlank(groups = NewManufacturerValidator.class)
    private String lastname;

    @NotBlank(groups = NewManufacturerValidator.class, message = "cannot be null during addition")
    @NationalityValid(groups = NewManufacturerValidator.class)
    private String nationality;

    @JsonProperty(access = Access.READ_ONLY)
    private List<Long> beerIds = new ArrayList<>();
}
