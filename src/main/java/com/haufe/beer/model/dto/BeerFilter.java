package com.haufe.beer.model.dto;

import com.haufe.beer.model.type.BeerType;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BeerFilter {
    
    private String name;
    
    private Double graduation;
    
    private BeerType beerType;
    
    private String nationality;

    private Long manufacturerId;
}
