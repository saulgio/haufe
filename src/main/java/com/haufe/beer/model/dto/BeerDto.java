package com.haufe.beer.model.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.haufe.beer.model.type.BeerType;
import com.haufe.beer.validator.NewBeerValidator;
import com.haufe.beer.validator.UpdateBeerValidator;

import lombok.Data;

@Data
public class BeerDto implements Serializable {

    private static final long serialVersionUID = 3868785667117791778L;

    @JsonProperty(access = Access.READ_ONLY)
    private Long id;

    @NotBlank(groups = NewBeerValidator.class, message = "cannot be null during addition")
    private String name;

    @NotNull(groups = NewBeerValidator.class, message = "cannot be null during addition")
    private Double graduation;

    @NotNull(groups = NewBeerValidator.class, message = "cannot be null during addition")
    private BeerType type;

    @NotBlank(groups = NewBeerValidator.class, message = "cannot be null during addition")
    private String description;

    @NotNull(groups = NewBeerValidator.class, message = "cannot be null during addition")
    @Null(groups = UpdateBeerValidator.class, message = "must be null during update")
    private Long manufacturerId;

    private String manufacturerType = null;
}
