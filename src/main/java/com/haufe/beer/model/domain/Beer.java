package com.haufe.beer.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.haufe.beer.model.type.BeerType;

import lombok.Data;

@Data
@Entity
@Table(name = "BEER")
public class Beer {
    
    @Column(name = "ID_PK")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "GRADUATION", nullable = false)
    private Double graduation;

    @Column(name = "TYPE", nullable = false)
    private String type;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAN_ID_FK", nullable = false)
    private Manufacturer manufacturer;

    @Column(name = "IMAGE")
    @Lob
    private byte[] image;

    public Beer(String name, double graduation, BeerType type, String description) {
        this.name = name;
        this.graduation = graduation;
        this.type = type.name();
        this.description = description;
    }

    public Beer() {}
}
