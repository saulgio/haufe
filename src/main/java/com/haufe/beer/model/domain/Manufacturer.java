package com.haufe.beer.model.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.haufe.beer.model.type.RoleType;

import lombok.Data;

@Data
@Entity
@Table(name = "MANUFACTURER")
public class Manufacturer {

    @Column(name = "ID_PK")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "FIRSTNAME", nullable = false)
    private String firstname;

    @Column(name = "LASTNAME", nullable = false)
    private String lastname;

    @Column(name = "NATIONALITY", nullable = false)
    private String nationality;

    @Column(name = "BEER_FK")
    @OneToMany(mappedBy = "manufacturer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Beer> beers = new ArrayList<>();

    @Column(name = "ROLE")
    private String role = RoleType.MANUFACTURER.name();

    public Manufacturer(String email, String password, String firstname, String lastname, String nationality, RoleType role) {
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.nationality = nationality;
        this.role = role.name();
    }

    public Manufacturer() {
    }
}
