package com.haufe.beer.model.type;

public enum RoleType {
    ADMIN, MANUFACTURER
}
