package com.haufe.beer.model.type;

public enum BeerType {
    ALE("Ale"), 
    LAGER("Lager"), 
    PORTER("Porter"), 
    STOUT("Stout"), 
    BLONDE_ALE("Blonde Ale"), 
    BROWN_ALE("Brown Ale"), 
    PALE_ALE("Pale Ale"), 
    INDIA_PALE_ALE("India Pale Ale"), 
    WHEAT("Wheat"), 
    PILSNER("Pilsner"), 
    SOUR_ALE("Sour Ale");

    private String name;

    private BeerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
