package com.haufe.beer.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 * Used to avoid DataIntegrityViolationException when the update operation is partial
 */
public class MapperHelper {
    
    private static String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();
    
        List<String> emptyNames = new ArrayList<>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
    
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
    
    // Use Spring BeanUtils to copy and ignore null using our function
    public static void map(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }
}