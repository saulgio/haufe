package com.haufe.beer.service.impl;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import com.haufe.beer.exception.BeerNotFoundException;
import com.haufe.beer.helper.MapperHelper;
import com.haufe.beer.model.domain.Beer;
import com.haufe.beer.model.domain.Manufacturer;
import com.haufe.beer.model.dto.BeerFilter;
import com.haufe.beer.repository.BeerRepository;
import com.haufe.beer.repository.ManufacturerRepository;
import com.haufe.beer.service.BeerService;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Service("beerService")
@Slf4j
public class BeerServiceImpl implements BeerService {

    @Resource
    private BeerRepository beerRepository;

    @Resource
    private ManufacturerRepository manufacturerRepository;

    @Override
    public List<Beer> getBeers(Pageable pageable) {
        return beerRepository.findAll(pageable).getContent();
    }

    @Override
    public List<Beer> getBeersByFilter(BeerFilter filter, Pageable pageable) {
        return beerRepository.findAllByFilter(filter, pageable);
    }

    @Override
    public Beer createBeer(Beer beer) {
        return beerRepository.save(beer);
    }

    @Override
    @Transactional
    public Beer updateBeer(Beer beer) {
        MapperHelper.map(beer, getById(beer.getId()));
        Manufacturer man = beerRepository.findById(beer.getId()).orElseThrow(BeerNotFoundException::new)
                .getManufacturer();
        beer.setManufacturer(man);
        return beerRepository.save(beer);
    }

    @Override
    @Transactional
    public void addImage(Long id, MultipartFile image) {
        Beer beer = getById(id);
        try {
            beer.setImage(image.getBytes());
            updateBeer(beer);
        } catch (IOException e) {
            log.error("error uploading image", e);
        }
    }

    @Override
    public Beer getById(Long id) {
        return beerRepository.findById(id).orElseThrow(BeerNotFoundException::new);
    }

    @Override
    public void deleteById(Long id) {
        beerRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        beerRepository.deleteAll();
    }
}
