package com.haufe.beer.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.haufe.beer.exception.ManufacturerNotFoundException;
import com.haufe.beer.helper.MapperHelper;
import com.haufe.beer.model.domain.Manufacturer;
import com.haufe.beer.repository.ManufacturerRepository;
import com.haufe.beer.service.ManufacturerService;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service("manufacturerService")
public class ManufacturerServiceImpl implements ManufacturerService {

    @Resource
    private ManufacturerRepository manufacturerRepository;

    @Override
    public List<Manufacturer> getManufacturers(Pageable pageable) {
        return manufacturerRepository.findAll(pageable).getContent();
    }

    @Override
    public Manufacturer createManufacturer(Manufacturer manufacturer) {
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    public Manufacturer updateManufacturer(Manufacturer manufacturer) {
        MapperHelper.map(manufacturer, getById(manufacturer.getId()));
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    public Manufacturer getById(Long id) {
        return manufacturerRepository.findById(id).orElseThrow(ManufacturerNotFoundException::new);
    }

    @Override
    public void deleteById(Long id) {
        manufacturerRepository.deleteById(id);
    }
}
