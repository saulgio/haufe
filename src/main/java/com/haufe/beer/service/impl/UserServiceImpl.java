package com.haufe.beer.service.impl;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import com.haufe.beer.model.domain.Manufacturer;
import com.haufe.beer.repository.ManufacturerRepository;
import com.haufe.beer.security.UserPrincipal;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("userService")
public class UserServiceImpl implements UserDetailsService {

    @Resource
    private ManufacturerRepository manufacturerRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.debug("load userdetails by email: {}", email);
        Manufacturer manufacturer = manufacturerRepository.getByEmail(email);
        List<GrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority("ROLE_" + manufacturer.getRole()));
        return new UserPrincipal(manufacturer.getEmail(), manufacturer.getPassword(), authorities, manufacturer.getId());
    }
}