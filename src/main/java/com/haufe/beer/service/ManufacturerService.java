package com.haufe.beer.service;

import java.util.List;

import com.haufe.beer.model.domain.Manufacturer;

import org.springframework.data.domain.Pageable;

public interface ManufacturerService {
    
    List<Manufacturer> getManufacturers(Pageable pageable);

    Manufacturer createManufacturer(Manufacturer manufacturer);

    Manufacturer updateManufacturer(Manufacturer manufacturer);

    Manufacturer getById(Long id);

    void deleteById(Long id);
}
