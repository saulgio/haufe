package com.haufe.beer.service;

import java.util.List;

import com.haufe.beer.model.domain.Beer;
import com.haufe.beer.model.dto.BeerFilter;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface BeerService {

    List<Beer> getBeers(Pageable pageable);

    List<Beer> getBeersByFilter(BeerFilter filter, Pageable pageable);

    Beer createBeer(Beer beer);

    Beer updateBeer(Beer beer);

    void addImage(Long id, MultipartFile image);

    Beer getById(Long id);

    void deleteById(Long id);

    void deleteAll();
}
