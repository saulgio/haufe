package com.haufe.beer.repository;

import java.util.List;

import com.haufe.beer.model.domain.Beer;
import com.haufe.beer.model.dto.BeerFilter;

import org.springframework.data.domain.Pageable;

public interface BeerRepositoryCustom {

    List<Beer> findAllByFilter(BeerFilter filter, Pageable pageable);
}
