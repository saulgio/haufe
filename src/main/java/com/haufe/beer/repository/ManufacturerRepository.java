package com.haufe.beer.repository;

import com.haufe.beer.model.domain.Manufacturer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long>, ManufacturerRepositoryCustom {

}
