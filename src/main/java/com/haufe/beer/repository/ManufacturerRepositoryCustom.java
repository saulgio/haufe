package com.haufe.beer.repository;

import com.haufe.beer.model.domain.Manufacturer;

public interface ManufacturerRepositoryCustom {
    
    Manufacturer getByEmail(String email);
}
