package com.haufe.beer.repository;

import com.haufe.beer.model.domain.Beer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BeerRepository extends JpaRepository<Beer, Long>, BeerRepositoryCustom {
    
}
