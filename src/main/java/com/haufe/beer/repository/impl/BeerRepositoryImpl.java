package com.haufe.beer.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.haufe.beer.model.domain.Beer;
import com.haufe.beer.model.dto.BeerFilter;
import com.haufe.beer.repository.BeerRepositoryCustom;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

public class BeerRepositoryImpl implements BeerRepositoryCustom {

    @Autowired
    private EntityManager em;

    @Override
    public List<Beer> findAllByFilter(BeerFilter filter, Pageable pageable) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Beer> cq = cb.createQuery(Beer.class);
        Root<Beer> root = cq.from(Beer.class);
        List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.isNotBlank(filter.getName())) {
            predicates.add(cb.like(cb.upper(root.get("name")), "%" + filter.getName().toUpperCase() + "%"));
        }

        if (filter.getGraduation() != null && filter.getGraduation() > 0) {
            predicates.add(cb.equal(root.get("graduation"), filter.getGraduation()));
        }

        if (filter.getBeerType() != null) {
            predicates.add(cb.equal(root.get("type"), filter.getBeerType().name()));
        }

        if (StringUtils.isNotBlank(filter.getNationality())) {
            predicates.add(cb.like(root.get("nationality"), filter.getNationality().toUpperCase()));
        }

        if (filter.getManufacturerId() != null) {
            root.fetch("manufacturer", JoinType.LEFT);
            predicates.add(cb.equal(root.get("manufacturer").get("id"), filter.getManufacturerId()));
        }

        cq.select(root).where(predicates.toArray(new Predicate[] {}));
        cq.orderBy(QueryUtils.toOrders(pageable.getSort(), root, cb));
        TypedQuery<Beer> query = em.createQuery(cq);
        query.setFirstResult(Math.toIntExact(pageable.getOffset()));
        query.setMaxResults(pageable.getPageSize());
        return query.getResultList();
    }

}
