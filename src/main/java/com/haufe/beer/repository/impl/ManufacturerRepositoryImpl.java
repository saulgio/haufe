package com.haufe.beer.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.haufe.beer.model.domain.Manufacturer;
import com.haufe.beer.repository.ManufacturerRepositoryCustom;

import org.springframework.beans.factory.annotation.Autowired;

public class ManufacturerRepositoryImpl implements ManufacturerRepositoryCustom {

    @Autowired
    private EntityManager em;
    
    @Override
    public Manufacturer getByEmail(String email) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Manufacturer> cq = cb.createQuery(Manufacturer.class);
        Root<Manufacturer> root = cq.from(Manufacturer.class);
        cq.select(root).where(cb.equal(root.get("email"), email));
        return em.createQuery(cq).getSingleResult();
    }
    
}
