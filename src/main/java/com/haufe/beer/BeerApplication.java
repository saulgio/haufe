package com.haufe.beer;

import java.util.Arrays;

import javax.annotation.Resource;

import com.haufe.beer.controller.BeerController;
import com.haufe.beer.exception.RestExceptionHandler;
import com.haufe.beer.model.domain.Beer;
import com.haufe.beer.model.domain.Manufacturer;
import com.haufe.beer.model.type.BeerType;
import com.haufe.beer.model.type.RoleType;
import com.haufe.beer.repository.BeerRepository;
import com.haufe.beer.repository.ManufacturerRepository;
import com.haufe.beer.security.AuthorizationHelper;
import com.haufe.beer.security.WebSecurity;
import com.haufe.beer.service.BeerService;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SpringBootApplication
@ComponentScan(basePackageClasses = { AuthorizationHelper.class, BeerController.class, BeerService.class, BeerRepository.class })
@Import({ WebSecurity.class, RestExceptionHandler.class })
public class BeerApplication {

	@Resource
	private ManufacturerRepository manufacturerRepository;

	@Resource
	private BeerRepository beerRepository;

	public static void main(String[] args) {
		SpringApplication.run(BeerApplication.class, args);
	}

	@Bean
	public CommandLineRunner preLoadMongo() {
		return args -> {
			if (manufacturerRepository.count() == 0) {
				initDB();
			}
		};
	}

	@Bean(name = "mapper")
	public ModelMapper mapper() {
		return new ModelMapper();
	}

	@Bean(name = "bCryptPasswordEncoder")
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build();
	}

	private void initDB() {
		Manufacturer admin = new Manufacturer("admin@test.com", bCryptPasswordEncoder().encode("admin"), "Admin",
				"Admin", "ITA", RoleType.ADMIN);
		Manufacturer manufacturer1 = new Manufacturer("joe@test.com", bCryptPasswordEncoder().encode("asd123"), "Joe",
				"Biden", "USA", RoleType.MANUFACTURER);
		Manufacturer manufacturer2 = new Manufacturer("angela@test.com", bCryptPasswordEncoder().encode("asd123"),
				"Angela", "Merkel", "DEU", RoleType.MANUFACTURER);
		Manufacturer manufacturer3 = new Manufacturer("boris@test.com", bCryptPasswordEncoder().encode("asd123"),
				"Boris ", "Johnson", "GBR", RoleType.MANUFACTURER);

		Beer beer1 = new Beer("Joy", 6.5, BeerType.LAGER, "Feel the joy!");
		beer1.setManufacturer(manufacturer1);
		Beer beer2 = new Beer("Happy", 6.4, BeerType.LAGER, "Will make you happy");
		beer2.setManufacturer(manufacturer1);
		Beer beer3 = new Beer("Glad", 6.4, BeerType.LAGER, "Glad to drink you");
		beer3.setManufacturer(manufacturer1);
		Beer beer4 = new Beer("Great", 4.8, BeerType.LAGER, "The best!");
		beer4.setManufacturer(manufacturer2);
		Beer beer5 = new Beer("Blue", 7.5, BeerType.LAGER, "Blue beer?");
		beer5.setManufacturer(manufacturer2);
		Beer beer6 = new Beer("Red", 5.2, BeerType.LAGER, "Classic red");
		beer6.setManufacturer(manufacturer2);
		Beer beer7 = new Beer("Green", 6.5, BeerType.LAGER, "The color of the nature");
		beer7.setManufacturer(manufacturer3);
		Beer beer8 = new Beer("Brown", 6.5, BeerType.LAGER, "Brown beer");
		beer8.setManufacturer(manufacturer3);
		Beer beer9 = new Beer("Gold", 4.2, BeerType.LAGER, "Precious as gold");
		beer9.setManufacturer(manufacturer3);

		manufacturerRepository.saveAll(Arrays.asList(admin, manufacturer1, manufacturer2, manufacturer3));
		beerRepository.saveAll(Arrays.asList(beer1, beer2, beer3, beer4, beer5, beer6, beer7, beer8, beer9));
	}
}
