package com.haufe.beer;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.haufe.beer.exception.ManufacturerNotFoundException;
import com.haufe.beer.exception.UnauthorizedException;
import com.haufe.beer.model.type.RoleType;
import com.haufe.beer.service.BeerService;
import com.haufe.beer.service.ManufacturerService;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class BeerApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Resource
	private ManufacturerService manufacturerService;

	@Resource
	private BeerService beerService;

	@Resource
	private ModelMapper mapper;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void getManufacturers() throws Exception {
		mvc.perform(get("/manufacturers").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].firstname", is("Admin"))).andExpect(jsonPath("$[3].lastname", is("Johnson")));
	}

	@Test
	void getManufacturerById() throws Exception {
		mvc.perform(get("/manufacturers/2").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nationality", is("USA")));

		assertEquals(manufacturerService.getById(2L).getRole(), RoleType.MANUFACTURER.name(), "Role doesn't match");
	}

	@Test
	@WithUserDetails(value = "admin@test.com")
	void deleteManufacturerAdmin() throws Exception {
		mvc.perform(delete("/manufacturers/2").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		assertThrows(ManufacturerNotFoundException.class, () -> manufacturerService.getById(2L));
	}

	@Test
	@WithUserDetails(value = "angela@test.com")
	void deleteManufacturerDifferentUser() throws Exception {
		try {
			mvc.perform(delete("/manufacturers/2").contentType(MediaType.APPLICATION_JSON));
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof UnauthorizedException);
		}
		assertEquals("Joe", manufacturerService.getById(2L).getFirstname());
	}

	@Test
	@WithUserDetails(value = "angela@test.com")
	void deleteManufacturerRightUser() throws Exception {
		mvc.perform(delete("/manufacturers/3").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		assertThrows(ManufacturerNotFoundException.class, () -> manufacturerService.getById(3L));
	}

	@Test
	@WithUserDetails(value = "admin@test.com")
	void addManufacturerAdmin() throws Exception {
		// map instead of new ManufacturerDto because the setter of email and psw is not
		// permitted as the attributes are WRITE_ONLY
		Map<String, String> dto = new HashMap<>();
		dto.put("email", "test@test.com");
		dto.put("password", "test");
		dto.put("firstname", "firstTest");
		dto.put("lastname", "lastTest");
		dto.put("nationality", "ITA");
		mvc.perform(post("/manufacturers").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated());
		assertEquals("test@test.com", manufacturerService.getById(5L).getEmail());
	}

	@Test
	void getBeers() throws Exception {
		mvc.perform(get("/beers").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].name", is("Joy")))
				.andExpect(jsonPath("$[8].description", is("Precious as gold")));
	}

	@Test
	@WithUserDetails(value = "admin@test.com")
	void getExternalBeers() throws Exception {
		mvc.perform(delete("/beers")).andExpect(status().isOk());
		assertTrue(beerService.getBeers(PageRequest.of(0, 100)).isEmpty(), "DB should be empty");
		mvc.perform(get("/beers")).andExpect(status().isOk()).andExpect(jsonPath("$").isNotEmpty());
	}

	@Test
	@WithUserDetails(value = "angela@test.com")
	void deleteBeerWrongManufacturer() throws Exception {
		try {
			mvc.perform(delete("/beers/2").contentType(MediaType.APPLICATION_JSON));
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof UnauthorizedException);
		}
	}

	@Test
	@WithUserDetails(value = "angela@test.com")
	void deleteBeerRightManufacturer() throws Exception {
		mvc.perform(delete("/beers/4")).andExpect(status().isOk());
	}

}
